<?php
/**
 * @file
 * custom_tpl.features.inc
 */

/**
 * Implements hook_node_info().
 */
function custom_tpl_node_info() {
  $items = array(
    'custom_tpl' => array(
      'name' => t('Custom tpl'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

<?php
/**
 * @file
 * custom_tpl.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function custom_tpl_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-custom_tpl-field_custom_tpl_node'.
  $field_instances['node-custom_tpl-field_custom_tpl_node'] = array(
    'bundle' => 'custom_tpl',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_custom_tpl_node',
    'label' => 'Node',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'options_select',
      'weight' => -3,
    ),
  );

  // Exported field_instance: 'node-custom_tpl-field_custom_tpl_node_sugestion'.
  $field_instances['node-custom_tpl-field_custom_tpl_node_sugestion'] = array(
    'bundle' => 'custom_tpl',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_custom_tpl_node_sugestion',
    'label' => 'Custom node tpl',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => -1,
    ),
  );

  // Exported field_instance: 'node-custom_tpl-field_custom_tpl_page_sugestion'.
  $field_instances['node-custom_tpl-field_custom_tpl_page_sugestion'] = array(
    'bundle' => 'custom_tpl',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_custom_tpl_page_sugestion',
    'label' => 'Custom page tpl',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => -2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Custom node tpl');
  t('Custom page tpl');
  t('Node');

  return $field_instances;
}
